#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <assert.h>

char* strlist(const Node * head) {
    const Node * orig = head;
    char* str = malloc(length(head) * 2 * sizeof(char) + 1); // reserve space
    char* writepos = str;
    for (head = orig; head != NULL; head = head->next) {
        sprintf(writepos, "%c ", head->data);
        writepos += 2;
    }
    if (str == writepos)  // list was empty
      str[0] = '\0';
    else
      *(writepos-1) = '\0';    // overwrite the last space with NULL
    return str;
} 

int main() {

  
  Node * mylist = NULL;    // must initialize for good results!!

  for (char c='e'; c >= 'a'; c--) 
    addFront(&mylist, c);
  
  assert(length(mylist) == 5);
  char* str = strlist(mylist);
  assert(strcmp(str,"a b c d e") == 0);
  printReverse(mylist);  // outputs e d c b a
  printf("\n");
  free(str);
    
  clearList(&mylist);


  /******  This section primarily tests deleteAfter and deleteFront. Uncomment after ex6-1 Part 2.
   */
  // test deleteFront, deleteAfter
  addFront(&mylist, 'A');
  addAfter(mylist, 'M');
  addAfter(mylist, 'H');
  addAfter(mylist, 'c');
  addAfter(mylist, 'S');
  addAfter(mylist, 'b');
  
  str = strlist(mylist);
  assert(strcmp(str, "A b S c H M") == 0);
  free(str);
  assert(deleteFront(&mylist) == 'A');
  str = strlist(mylist);
  assert(strcmp(str, "b S c H M") == 0);
  assert(length(mylist) == 5);
  free(str);

  assert(deleteAfter(mylist->next) == 'c');
  str = strlist(mylist);
  assert(strcmp(str, "b S H M") == 0);
  assert(length(mylist) == 4);
  free(str);
  clearList(&mylist);
  assert(mylist == NULL);




  /******  This section primarily tests removeAll. Uncomment after ex6-1 Part 4.
   */
  // test removeAll
  for (char c = 'a'; c < 'j'; c++) {
    addFront(&mylist, c);
  }
  for (char c = 'i'; c >= 'c'; c--) {
    addFront(&mylist, c);
  }
  str = strlist(mylist);
  assert(strcmp(str, "c d e f g h i i h g f e d c b a") == 0);
  free(str);
  removeAll(&mylist, 'i');
  str = strlist(mylist);
  assert(strcmp(str, "c d e f g h h g f e d c b a") == 0);
  free(str);
  removeAll(&mylist, 'i');
  str = strlist(mylist);
  assert(strcmp(str, "c d e f g h h g f e d c b a") == 0);
  free(str);
  removeAll(&mylist, 'c');
  str = strlist(mylist);
  assert(strcmp(str, "d e f g h h g f e d b a") == 0);
  free(str);
  removeAll(&mylist, 'a');
  str = strlist(mylist);
  assert(strcmp(str, "d e f g h h g f e d b") == 0);
  free(str);
  clearList(&mylist);
  assert(mylist == NULL);




  /******  This section primarily tests insert. Uncomment after ex6-1 Part 6.
   */
  // test insert
  for (char c = 'A'; c < 'F'; c++) {
    insert(&mylist, c);
  }
  str = strlist(mylist);
  assert(strcmp(str, "A B C D E") == 0);
  free(str);
  clearList(&mylist);    
  for (char c = 'E'; c > 'A'; c--) {
    insert(&mylist, c);
  }
  str = strlist(mylist);
  assert(strcmp(str, "B C D E") == 0);
  free(str);
  insert(&mylist, 'J');
  insert(&mylist, 'D');
  insert(&mylist, 'A');
  str = strlist(mylist);
  assert(strcmp(str, "A B C D D E J") == 0);
  free(str);
  clearList(&mylist);




  printf("all tests passed!\n");
  return 0;

}


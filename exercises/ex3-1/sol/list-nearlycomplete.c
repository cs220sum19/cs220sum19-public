#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

// put char into a newly created node
Node * createNode(char ch) {
  Node * node = (Node *) malloc(sizeof(Node));
  assert(node); //confirm malloc didn't fail

  node->data = ch;
  node->next = NULL;
  return node;
}

// output the list iteratively
void printList(const Node * cur) {
  while (cur != NULL) {
    printf("%c ", cur->data);
    cur = cur->next;  // advance to next node
  }
}

// output the list recursively
void printRec(const Node * head) {
  if (head != NULL) {
    printf("%c ", head->data);
    printRec(head->next);
  }
}

// output the list in reverse (recursive)
void printReverse(const Node * head) {
  if (head != NULL) {  // equivalent: if (head)
    printReverse(head->next);
    printf("%c ", head->data);
  }
}

// count and return the number of elements in the List (recursive)
long length(const Node * head) {
  if (head == NULL)
    return 0;
  return 1 + length(head->next);
}

// get rid of (deallocate) entire list, recursively from end to start
void clearList(Node **lptr) {
  if (*lptr != NULL) {
    clearList( &((*lptr)->next));
    free(*lptr);
    *lptr = NULL;
  }
}

// add char immediately after existing node
void addAfter(Node *node, char val) {
  if (node == NULL)
    return;
  Node *n = createNode(val);
  n->next = node->next;
  node->next = n;
}

// add char to beginning of list
void addFront(Node **lptr, char val) {
  Node *n = createNode(val);
  n->next = *lptr;
  *lptr = n;
}

// EXERCISE
// delete node after current, return char
char deleteAfter(Node *node) {
  if (node == NULL || node->next == NULL)  // order matters!
    return 0;
  Node *temp = node->next;  // the one to be deleted
  node->next = temp->next;  // bypass temp node
  temp->next = NULL;        // disconnect from list
  char val = temp->data;
  free(temp);               // get node memory space back
  return val;
}

// EXERCISE
// delete first node, if any, return char
char deleteFront(Node **lptr) {
  if (*lptr == NULL) 
    return 0;

  Node *temp = *lptr;    // first node
  *lptr = temp->next;    // advance actual head
  temp->next = NULL;     // disconnect from list
  char val = temp->data;
  free(temp);            // get node memory space back
  return val;
}

// EXERCISE
// remove all occurrences of a particular character
void removeAll(Node **lptr, char val) {
  if (!lptr || !(*lptr)) {
    return;  //do nothing since list is already empty
  }

  // check if need to (keep) removing first node (special since requires us
  // to adjust head pointer, a.k.a. *lptr)
  while ((*lptr)->data == val) {  
    Node * temp = (*lptr);     // retain address of node to be deleted
    *lptr = (*lptr)->next;     // advance head pointer past node to be deleted
    free(temp);                // deallocate the deleted node
  }

  // now start cur at first (remaining) node, and look ahead to next node
  Node *cur = *lptr;

  while (cur->next) {  // stop when we reach final node

    // if node ahead of us holds val, remove it
    if (cur->next->data == val) {
      Node * temp = (cur->next);     // retain address of node to be deleted
      cur->next = (cur->next)->next; // advance cur->next past node to be deleted
      free(temp);                    // deallocate the deleted node     
    } else {
      // advance to next node since didn't make a deletion
      cur = cur->next;
    }
  }
}

// EXERCISE
// insert in order (assumes list is ordered)!!
// return the address of the node containing the character inserted
Node * insert(Node **lptr, char val) {

  assert(lptr);  // don't proceed if lptr is NULL

  // create new node to hold val
  Node *newNode = malloc(sizeof(Node));
  newNode->data = val;
  newNode->next = NULL;

  // check for empty list
  if (!(*lptr)) {
    *lptr = newNode;
    return newNode; 
  }

  // check whether newNode goes at start of list
  if (val <= (*lptr)->data) {
    // insert at front
    newNode->next = *lptr;
    *lptr = newNode;
    return newNode;

  } else {  

    // figure out where it goes later in list
    Node *cur = *lptr;
    // advance cur only until newNode should go after it
    while (cur->next && cur->next->data < val) {
      cur = cur->next;
    }
    // attach newNode after cur
    newNode->next = cur->next;
    cur->next = newNode;
    return newNode;
  }
}



// insert char at end of list
void addTail(Node **lptr, char val) {
  Node *n = createNode(val);
  if (*lptr == NULL) {
    (*lptr) = n;
    return;
  }
  Node *ptr = *lptr;
  while(ptr->next != NULL) {
    ptr = ptr->next;
  }
  ptr->next = n;
}


// insert char at end of list (recursive)
void addTailRec(Node **lptr, char val) {
  if ((*lptr) != NULL) {
    addTailRec( &((*lptr)->next), val);
  } else {
    Node *n = createNode(val);
    (*lptr) = n;
  }
}

// find a value in the list, return pointer to containing node, or NULL
Node* find(Node * node, char val) {
  if (node == NULL) {
    return NULL;
  }
  if (node->data == val) {
    return node;
  } else {
    return find(node->next, val);
  }
}

// delete 1st occurrence of char from list if there, return 1 success, 0 failure
int delete(Node **lptr, char val) {
  // not implemented
  return 0;
}

// recursive version of delete first occurrence
int deleteRec(Node **lptr, char val)  {
  if ((*lptr) == NULL) {
    return 0;
  }
  if ((*lptr)->data == val) {
    // do delete stuff

    Node *tmp = (*lptr)->next;
    free(*lptr);
    *lptr = tmp;
    return 1;
  } else {
    return deleteRec(&((*lptr)->next), val);
  }
}

// replace first occurrence of old with new, return 1 success, 0 failure
int replace(Node * head, char old, char new) {
  if (! head) // empty/end of list
    return 0;
  if (head->data == old) {
    head->data = new;
    return 1;
  }
  else // recurse
    return replace(head->next, old, new);  
}


// 601.220 - example of inheritance, virtual functions, dynamic binding

#include "Aclass.h"
#include "Bclass.h"
#include <iostream>


using std::cout;
using std::endl;

int main (void) {
  A aobj(10);
  B bobj(20);
  

  // Part 2 
  cout << aobj.toString() << endl;
  cout << bobj.toString() << endl;
  aobj.seta(13);
  aobj = bobj;
  cout << aobj.toString() << endl;
  bobj.setb(27);
  bobj = aobj;
  cout << bobj.toString() << endl;

  // Part 3
  cout << aobj.fun();
  bobj.seta(1);
  bobj.setd(2);
  cout << bobj.fun() << endl;
  bobj.setb(2);
  cout << bobj.fun() << endl;
  
  return 0;
  
}



#ifndef MY_SET_H
#define MY_SET_H

#include <iostream>
#include <ostream>
#include "my_node.h"

/* Class to represent a TEMPLATED mathematical set of ints (like 
   std::set<int>, but we're writing this one ourselves).  

   Recall that set objects may never contain duplicate values, 
   so we must avoid adding new items that are equal to existing 
   items in the set.  To facilitate checking for duplicates, we 
   store the items in a SORTED linked list of my_node<T> type. 
   The sort is in ascending order by < on type T.  Avoiding
   duplicates requires the == operator to be defined on T as well.
*/
template<typename T>
class my_set {

 private:

  my_node<T>* head;
  int         size;

 public:

  //constructor to create an empty list
  my_set(): head(nullptr), size(0) { }

  //copy constructor to make a "deep copy" of the set
  my_set(const my_set& other);

  //destructor
  ~my_set();

  //report the current size of the set (i.e. number of elements in it)
  int get_size() const { return size; }
  
  //remove all existing items from set
  void clear();

  //insert the given item into the set, provided it's not a duplicate
  //return true if insertion successful, false otherwise
  bool add(T new_value);
 

  // alternative way to call add, returning this updated my_set
  my_set& operator+=(T new_value);


  // overload the assignment operator to make a deep copy and return
  // a reference to this updated my_set
  my_set& operator=(const my_set& other);


  //output items in set, comma-and-space-separated within curly braces
  //E.g.  {1, 2, 3}  or {hello, world} or {}
  //
  //Note that we needed to introduce a new letter, U, to represent the
  //template parameter for this method, since this isn't being defined
  //as a member of the class. In a sense, the name T is already in use
  //in the context of the class, and we don't want to have a conflict.
  template<typename U>
  friend std::ostream& operator<<(std::ostream& os, const my_set<U>& s);


};


/* 
   Since this is a templated class, any file using it needs the full
   method definitions, not just the header. So by writing the line below,
   we dump the contents of the .inc file here, so that main is spared
   from the need to write two #include statements (the .h and the .inc).
   It can just include the .h file, the method definitions will also come
   along for the ride.
*/
#include "my_set.inc"

#endif

//my_set.inc

/*
   This file provides defintions for the my_set<T> class.

   This file is named my_set.inc, not my_set.cpp, since it will actually
   be #included in other files.  Our convention is to avoid including
   files with .cpp extensions, so we renamed this one to indicate its role.

   We commented out the line below, since this file gets included at the
   bottom of my_set.h, and is never included anywhere or compiled on its own.
 */   
//#include "my_set.h"   //no longer needed!



//copy constructor to make a "deep copy" of the set
template<typename T>
my_set<T>::my_set(const my_set<T>& orig): head(nullptr), size(0){  

  //the initializer list above completed the copy constructor's job
  //if orig happens to be empty, so only proceed if orig is non-empty
  if (orig.head != nullptr) {  

    //create new first node and make head point to it
    head = new my_node<T>(orig.head->get_data());
    size++;

    //for each remaining node, make a copy and add to list
    my_node<T>* o_temp = orig.head;
    my_node<T>* n_temp = head;
    while (o_temp->get_next() != nullptr) {

      //we'll assume here that calls to new don't fail
      n_temp->set_next(new my_node<T>(o_temp->get_next()->get_data()));

      //update size instance variable to reflect that we've added a node
      size++;  

      //advance the pointer in old list and the pointer in new list
      o_temp = o_temp->get_next();
      n_temp = n_temp->get_next();
    }
  }
}

//destructor takes advantage of the existing clear method
template<typename T>
my_set<T>::~my_set<T>(){
  clear();
}

//remove all existing items from set, taking care to deallocate each one
template<typename T>
void my_set<T>::clear(){
  //deallocate all nodes in the list
  my_node<T> *cur = head;
  while (cur) {
	my_node<T>* temp = cur;
	cur = cur->get_next();
	delete temp;
  }
  //reset instance variables
  head = nullptr;
  size = 0;
}

//Insert the given item into the set, provided it's not a duplicate
//return true if insertion successful, false otherwise
//RESTRICTION: use only < and == on the data within nodes (not <=, >, >=, !=)
template<typename T>
bool my_set<T>::add(T new_value) {

  //first, abort if new_value would be a duplicate when added to set
  //(This is less efficient than it could be - an improved version
  //could just check for a duplicate element during our later loop over
  //the set)
  my_node<T>* cur = head;
  while (cur) {
    if (cur->get_data() == new_value) {  //using operator== on T type args
      return false;
    }
    cur = cur->get_next();
  }

  //next, create new node to hold the new value
  my_node<T>* new_node = new my_node<T>(new_value);
  if (!new_node) {  //allocation failed!
	return false; 
  }

  //if set is empty, then add new node at head
  if (!head) { 
	head = new_node;
	size++;
	return true;
  }

  //check if new value would be smallest, then add at head
  if (new_value < head->get_data()) { //using operator< on T type args
	head = new_node;
	size++;
	return true;
  }

  //new_node should go somewhere after 1st node, so let's locate
  //first node whose data larger than new_node's.  But we must
  //be careful not to fall off end of list!
  cur = head;

  //So we'll stop looping when either cur is pointing to last node
  //in the list, or cur has advanced as far into this list as it can,
  //while still pointing to a node with a value smaller than new one
  while (cur->get_next() && (cur->get_next()->get_data() < new_value)) {
    cur = cur->get_next();
  }
  new_node->set_next(cur->get_next());  //may be nullptr, but that's ok too
  cur->set_next(new_node);
  size++;

return true;
}
 


template<typename T>
my_set<T>& my_set<T>::operator+=(T new_value) {
  this->add(new_value);  //code reuse!  we ignore add's return value
  return *this;  
}



/*
   Writing operator= is similar to writing the copy constructor in
   that it should create a deep copy, but we also have two additional
   issues to consider, because in cases where we're using operator=
   to assign a value to lhs,it must be the case that lhs was already
   constructed.  (If it wasn't, then we'd use the copy constructor!)
   So:
   1) we have to deallocate the old list of nodes that is getting
      replaced by this assignment, and
   2) since users sometimes write assignment statements such as
        a = a;  //assign a value to itself
      we should be careful not to destroy that object!
 */
template<typename T>
my_set<T>& my_set<T>::operator=(const my_set<T>& other) {

  //if lhs and rhs refer to same heap memory, don't make modifications
  if (other.head == head && other.size == size) { 
    return *this;  //do nothing; don't want to deallocate the object!
  }

  //deallocate the old list of nodes that's getting replaced
  this->clear();  

  my_set<T> temp(other);     //use copy constructor
  this->head = temp.head;    //then assign individual fields
  this->size = temp.size;

  //lifetime of local variable temp is about to end
  temp.head = nullptr; //make sure deallocation of temp doesn't kill *this
  temp.size = 0;       //for completeness in case destructor implementation uses it

  return *this;  //returning the assigned value allows for chaining
}



/*
   Note: we don't prefix the name of operator<< definition with my_set<T>::,
   since it is not actually a full-fledged member of the class.  It's
   only a friend.  Also, the word 'friend' doesn't appear here; it only
   belongs in the class defintion in my_set.h.
  
   Further, note that although the friend declaration in my_set.h needed
   us to use U instead of T, either one will work here.  In this file,
   we're no longer under the influence of the template <typename T> clause
   at the start of the class definition.
*/
template<typename T>
std::ostream& operator<<(std::ostream& os, const my_set<T>& s) {
  os << "{";  //indicate start of set

  //loop over all nodes in set s and print data that's in each one
  my_node<T>* temp = s.head;
  while (temp != nullptr) {
    os << temp->get_data();   //using operator<< with T type as second arg
    if (temp->get_next() != nullptr) {
      os << ", ";
    }
    temp = temp->get_next();
  }

  os << "}";  //indicate end of set
  return os;  //returning the modified stream allows chaining
}




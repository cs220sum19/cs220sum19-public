#include "int_set.h"

using std::ostream;

//copy constructor to make a "deep copy" of the set
int_set::int_set(const int_set& orig): head(nullptr), size(0){  

  if (orig.head != nullptr) {  //orig is not empty

    //create new first node and make head point to it
    head = new int_node(orig.head->get_data());
    size++;

    //for each remaining node, make a copy and add to list
    int_node* o_temp = orig.head;
    int_node* n_temp = head;
    while (o_temp->get_next() != nullptr) {
      n_temp->set_next(new int_node(o_temp->get_next()->get_data()));
      size++;
      o_temp = o_temp->get_next();
      n_temp = n_temp->get_next();
    }
  }
}

//destructor
int_set::~int_set(){
  clear();
}

//remove all existing items from set
void int_set::clear(){
  //deallocate all nodes in the list
  int_node *cur = head;
  while (cur) {
	int_node* temp = cur;
	cur = cur->get_next();
	delete temp;
  }
  //reset instance variables
  head = nullptr;
  size = 0;
}

//insert the given item into the set, provided it's not a duplicate
//return true if insertion successful, false otherwise
//RESTRICTION: use only < and == on the data within nodes (not <=, >, >=, !=)
bool int_set::add(int new_value) {

  //first, abort if new_value would be a duplicate when added to set
  int_node* cur = head;
  while (cur) {
    if (cur->get_data() == new_value) {
      return false;
    }
    cur = cur->get_next();
  }

  //next, create new node to hold the new value
  int_node* new_node = new int_node(new_value);
  if (!new_node) {  //allocation failed!
	return false; 
  }

  if (!head) { //list is currently empty
	head = new_node;
	size++;
	return true;
  }
  if (new_value < head->get_data()) { //new_node is smallest
        new_node->set_next(head);
        head = new_node;
	size++;
	return true;
  }

  //new_node should go somewhere after 1st node, so let's locate
  //first node whose data larger than new_node's.  But we must
  //be careful not to fall off end of list!
  cur = head;
  //So we'll stop looping when either cur is pointing to last node
  //in the list, or cur has advanced as far into this list as it can,
  //while still pointing to a node with a value smaller than new one
  while (cur->get_next() && (cur->get_next()->get_data() < new_value)) {
    cur = cur->get_next();
  }
  new_node->set_next(cur->get_next());  //may be nullptr, but that's ok too
  cur->set_next(new_node);
  size++;
  return true;
}
 


ostream& operator<<(ostream& os, const int_set& s) {

  os << "{";  //indicate start of set

  //loop over all nodes in set s and print data at each one
  int_node* temp = s.head;
  while (temp != nullptr) {
    os << temp->get_data();
    if (temp->get_next() != nullptr) {
      os << ", ";
    }
    temp = temp->get_next();
  }

  os << "}";  //indicate end of set
  return os;
}



int_set& int_set::operator+=(int new_value) {
  this->add(new_value);
  return *this;
}


int_set& int_set::operator=(const int_set& other) {
  if (other.head == head && other.size == size) {
    return *this;  //do nothing
  }
  this->clear();  //deallocate existing set

  int_set temp(other);     //use copy constructor
  this->head = temp.head;  //then assign individual fields
  this->size = temp.size;
  temp.head = nullptr;  //make sure deallocation of temp doesn't kill *this
  temp.size = 0;

  return *this;
}
